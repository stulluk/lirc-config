# Hiremco - Seoul DVB STB LIC Config

```
# Please take the time to finish this file as described in
# https://sourceforge.net/p/lirc-remotes/wiki/Checklist/
# and make it available to others by sending it to
# <lirc@bartelmus.de>
#
# This config file was automatically generated
# using lirc-0.9.5-devel(default) on Tue May 24 07:03:39 2016
# Command line used: -n --driver default --device /dev/lirc0 --plugindir=../plugins/.libs
# Kernel version (uname -r): 3.4.104-sertac++
#
# Remote name (as of config file): Hiremco
# Brand of remote device, the thing you hold in your hand:
# Remote device model nr:
# Remote device info url:
# Does remote device has a bundled capture device e. g., a
# usb dongle? :
# For bundled USB devices: usb vendor id, product id
# and device string (use dmesg or lsusb):
# Type of device controlled
# (TV, VCR, Audio, DVD, Satellite, Cable, HTPC, ...) :
# Device(s) controlled by this remote:

begin remote

name Hiremco
bits 32
flags SPACE_ENC|CONST_LENGTH
eps 30
aeps 100

header 9063 4494
one 604 1655
zero 604 525
ptrail 601
repeat 9063 2238
gap 108481
toggle_bit_mask 0x0
frequency 38000

begin codes
power 0x80BF3BC4 0x7E933848
chup 0x80BF53AC 0x7E933848
chdown 0x80BF4BB4 0x7E933848
left 0x80BF9966 0x7E933848
right 0x80BF837C 0x7E933848
ok 0x80BF738C 0x7E933848
menu 0x80BFA956 0x7E933848
exit 0x80BFA35C 0x7E933848
f1 0x80BFD926 0x7E933848
f2 0x80BF59A6 0x7E933848
info 0x80BF0BF4 0x7E933848
1 0x80BF49B6 0x7E933848
2 0x80BFC936 0x7E933848
3 0x80BF33CC 0x7E933848
4 0x80BF718E 0x7E933848
5 0x80BFF10E 0x7E933848
6 0x80BF13EC 0x7E933848
7 0x80BF51AE 0x7E933848
8 0x80BFD12E 0x7E933848
9 0x80BF23DC 0x7E933848
0 0x80BFE11E 0x7E933848
mute 0x80BF39C6 0x7E933848
back 0x80BF41BE 0x7E933848
red 0x80BF6996 0x7E933848
green 0x80BF43BC 0x7E933848
yellow 0x80BFC33C 0x7E933848
blue 0x80BF21DE 0x7E933848
epg 0x80BF6B94 0x7E933848
sat 0x80BF916E 0x7E933848
end codes

end remote
```