# Digiturk LIRC config file

```
# This config file was automatically generated
 # using lirc-0.9.5-devel(default) on Tue May 24 08:13:40 2016
 # Command line used: -n --driver default --device /dev/lirc0 --plugindir=../plugins/.libs
 # Kernel version (uname -r): 3.4.104-sertac++
 #
 # Remote name (as of config file): Digiturk
 
begin remote
 
name Digiturk
 bits 13
 flags RC5|CONST_LENGTH
 eps 30
 aeps 100
 
one 918 853
 zero 918 853
 plead 908
 gap 113923
 min_repeat 1
# suppress_repeat 1
# uncomment to suppress unwanted repeats
 toggle_bit_mask 0x800
 frequency 38000
 
begin codes
 power 0x138C
 volumeup 0x13B1
 volumedown 0x13B2
 channelup 0x1392
 channeldown 0x13AE
 ok 0x1391
 exit 0x13AB
 back 0x1390
 1 0x1388
 2 0x1382
 3 0x1383
 4 0x1384
 5 0x1385
 6 0x1386
 7 0x1387
 8 0x1381
 9 0x1389
 0 0x1380
 red 0x138D
 green 0x13A0
 yellow 0x13B9
 blue 0x13B4
 rec 0x13B7
 stop 0x13B6
 menu 0x13AA
 up 0x138A
 down 0x13AD
 left 0x1396
 right 0x13AF
 mute 0x1397
 guide 0x1395
 mode 0x13B8
 pause 0x13B5
 end codes
 
end remote
```